const express = require("express");
const userData = require("../../data");
const router = express.Router();

router.get("/users", (req, res) => {
    let age = req.query.age;
    if (age == null) {
        res.status(200).json(
            userData
        )
    } else {
        let queryResult = []
        for (var user of userData) {
            if (parseInt(user.age) > parseInt(age)) {
                queryResult.push(user);
            }
        }
        if (queryResult.length == 0) {
            res.status(404).json({
                message: `not found user`
            })
        } else {
            res.status(200).json(
                queryResult
            )
        }

    }
});

router.get("/users/:userid", (req, res) => {
    let userId = req.params.userid.trim();
    if (isNaN(parseInt(userId))) {
        res.status(400).json({
            message: "bad request"
        })
    } else {
        let paramResult = [];
        for (var user of userData) {
            if (user.id == userId) {
                paramResult.push(user);
            }
        }
        if (paramResult.length == 0) {
            res.status(404).json({
                message: `not found user`
            })
        } else {
            res.status(200).json(
                paramResult
            )
        }
    }

});

router.post("/users", (req, res) => {
    let body = req.body;
    res.status(201).json({
        message: `create user successful`,
        body: body
    })
});

router.put("/users/:userid", (req, res) => {
    let userId = req.params.userid;
    let body = req.body;
    res.status(200).json({
        message: `update user with id ${userId}`,
        
        body: {userId, ...body}
    })
});

router.delete("/users/:userid", (req, res) => {
    let userId = req.params.userid;
    res.status(200).json({
        message: `delete user with id : ${userId}`
    })
});

module.exports = router;