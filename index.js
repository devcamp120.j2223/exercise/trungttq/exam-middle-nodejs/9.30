const express = require("express");
const userData = require("./data");
const userRouter = require("./app/router/userRouter");

const app = express();
const port = 8000;

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}))


app.use("/", userRouter);
app.listen(port, () => {
    console.log(`app running at port ${port}`);
})